import zoom_user_functions
import getpass

#Fetch a JWT token, given you have the Zoom account JWT API Key and API Secret 

api_key = getpass.getpass(prompt="API Key ")
api_secret = getpass.getpass(prompt="API Secret ")

token = zoom_user_functions.get_jwt_token(api_key, api_secret, interval=3600)



#fetch all active zoom users and their basic account details

zoom_user_functions.get_zoom_users(token)



#fetch a given zoom user and their basic account details by email

zoom_user_functions.get_zoom_user(user_id="aexample@upenn.edu",token=token)



#fetch a given zoom user and their basic account details by zoom id.

user_id = "zooooooom_random_id" #get the user account details to identify this value
zoom_user_functions.get_zoom_user(user_id=user_id,token=token)



#Update a zoom user's email address
#Needs more testing but believe it revokes any authorization types beyond WorkEmail (Non-SSO login)

new_email = "bexample@upenn.edu"
user_id = "zooooooom_random_id" #get the user account details to identify this value
zoom_user_functions.update_zoom_email(user_id=user_id, new_email=new_email, token=token)



#Pre-provision an SSO User that doesn't already have a Zoom account

email = "aexample@upenn.edu"
first_name = "A" #Wharton generally sources from PennGroups Webservices for this info
last_name = "Example" #Wharton generally sources from PennGroups Webservices for this info
zoom_user_functions.create_zoom_sso_user(email=email, first_name=first_name, last_name=last_name, token=token)



#Invite user that may already have a Zoom account

email = "aexample@upenn.edu"
first_name = "A" #Wharton generally sources from PennGroups Webservices for this info
last_name = "Example" #Wharton generally sources from PennGroups Webservices for this info
zoom_user_functions.create_zoom_user(email=email, first_name=first_name, last_name=last_name, token=token)



#Invite a user that doesn't have a Zoom account, and append SSO bit
#Still needs testings

email = "aexample@upenn.edu"
first_name = "A" #Wharton generally sources from PennGroups Webservices for this info
last_name = "Example" #Wharton generally sources from PennGroups Webservices for this info
zoom_user_functions.invite_zoom_sso_user(email=email, first_name=first_name, last_name=last_name, token=token)



#Revoke work email auth and enable SSO
#For invited users this needs to be done to ensure they can't bypass PennKey Lifecycles

user_id = "zooooooom_random_id" #get the user account details to identify this value
zoom_user_functions.set_zoom_sso(user_id=user_id,token=token)



#Get a User's SCIM account details, includes information on "linked accounts" as displayed in the GUI

user_id = "zooooooom_random_id" #get the user account details to identify this value
zoom_user_functions.get_zoom_scim_user(user_id=user_id, token=token)



#Boot a Zoom user from the enterprise account
#Lets them take all their stuff with 'em.

user_id = "zooooooom_random_id" #get the user account details to identify this value
zoom_user_functions.unlink_zoom_user(user_id=user_id,token=token)