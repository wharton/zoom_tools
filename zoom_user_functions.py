import requests
import time
import datetime
import getpass
import json
import jwt
import logging
import os

#logging bits for loggering
logging.basicConfig(level=os.environ.get("LOGLEVEL", "DEBUG"))
logger = logging.getLogger("params")

def get_jwt_token(api_key, api_secret, interval=60):
    interval = int(interval)
    payload = {
        "iss": api_key, 
        'exp': (datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(seconds=interval)).timestamp()
    }

    token = jwt.encode(
        payload,
        api_secret,
        algorithm='HS256'
    ).decode('utf-8')

    return token

def get_zoom_users(token, page_number=1, page_size=50, status="active", base_url="https://api.zoom.us", user_count=0):
    headers = {
        'authorization': "Bearer {0}".format(token),
        'content-type': "application/json"
    }

    api_url = "{0}/v2/users".format(base_url)
    params = {}
    if status:
        params['status'] = status
    if page_number:
        params['page_number'] = page_number
    if page_size:
        params['page_size'] = page_size
    users = None
    user_count = 0
    total_users = page_size
    logger.debug("Initial user count {0} and default total_users value {1}".format(user_count, total_users))
    try:
        while user_count < total_users:
            logger.debug("User count: {0} <= total_users: {1}".format(user_count, total_users))
            results = requests.get(
                url=api_url,
                headers=headers,
                params=params
            )
            params['page_number'] = params['page_number'] + 1
            if users:
                logger.debug("Appending to users object")
                users = users + results.json()['users']
            else:
                total_users = results.json()['total_records']
                logger.info("Total Users: {0}".format(total_users))
                logger.debug("Initial Population of users object")
                users = results.json()['users']
                
            user_count = len(users)
    except:
        raise ValueError
    if users:
        return users
    else:
        logger.warning("Returned No Users")
    

def get_zoom_user(user_id, token, base_url="https://api.zoom.us"):
    headers = {
        'authorization': "Bearer {0}".format(token),
        'content-type': "application/json"
    }
    query_url = "{0}/v2/users/{1}".format(base_url,user_id)
    try:
        results = requests.get(url=query_url, headers=headers)
    except:
        raise ValueError
    return results.json()

def update_zoom_email(user_id, new_email, token, base_url="https://api.zoom.us"):
    headers = {
        'authorization': "Bearer {0}".format(token),
        'content-type': "application/json"
    }
    payload = {
        'email':new_email
    }
    api_url = "{0}/v2/users/{1}/email".format(base_url,user_id)
    try:
        results = requests.put(url=api_url, headers=headers, data=json.dumps(payload))
    except:
        raise ValueError
    return results

def create_zoom_sso_user(email, first_name, last_name, token, base_url="https://api.zoom.us"):
    headers = {
        'authorization': "Bearer {0}".format(token),
        'content-type': "application/json"
    }
    payload = {
        'action': 'ssoCreate',
        'user_info': {
            'email':email,
            'type': 2,
            'first_name': first_name,
            'last_name': last_name
        }
    }
    api_url = "{0}/v2/users".format(base_url)
    try:
        results = requests.post(url=api_url, headers=headers, data=json.dumps(payload))
    except:
        raise ValueError
    return results

def create_zoom_user(email, first_name, last_name, token, base_url="https://api.zoom.us"):
    headers = {
        'authorization': "Bearer {0}".format(token),
        'content-type': "application/json"
    }
    payload = {
        'action': 'create',
        'user_info': {
            'email':email,
            'type': 2,
            'first_name': first_name,
            'last_name': last_name
        }
    }
    api_url = "{0}/v2/users".format(base_url)
    try:
        results = requests.post(url=api_url, headers=headers, data=json.dumps(payload))
    except:
        raise ValueError
    return results

def invite_zoom_sso_user(email, first_name, last_name, token, base_url="https://api.zoom.us"):
    headers = {
        'authorization': "Bearer {0}".format(token),
        'content-type': "application/json"
    }
    payload = {
        'action': 'create',
        'user_info': {
            'email':email,
            'type': 2,
            'first_name': first_name,
            'last_name': last_name
        }
    }
    api_url = "{0}/v2/users".format(base_url)
    try:
        results = requests.post(url=api_url, headers=headers, data=json.dumps(payload))
    except:
        raise ValueError
    if results.status_code == 201:
        logger.info("Invite user successfully, attempting to configure SSO")
        user_id = results.json()['id']
        results = set_zoom_sso(user_id, token)
        if results.status_code == 201:
            logger.info("Configured SSO on account user id: {0}".format(user_id))
        else:
            logger.warning("Failed to configure SSO on account user id: {0}".format(user_id))
            logger.warning("Review get_zoom_scim_user and apply set_zoom_sso function or like process later")
    return results

def unlink_zoom_user(user_id, token, base_url="https://api.zoom.us"):
    headers = {
        'authorization': "Bearer {0}".format(token),
        'content-type': "application/json"
    }
    payload = {
        'action': 'disassociate'
    }
    api_url = "{0}/v2/users/{1}".format(base_url,user_id)
    try:
        results = requests.delete(url=api_url, headers=headers, data=json.dumps(payload))
    except:
        raise ValueError
    return results

def set_zoom_sso(user_id, token, sso=True, work_email=False, base_url="https://api.zoom.us"):
    headers = {
        'authorization': "Bearer {0}".format(token),
        'content-type': "application/json"
    }
    payload = {
        "schemas": [
            "urn:us:zoom:scim:schemas:extension:1.0:ZoomUser"
        ],
        "loginType": {
            "sso": sso,
            "workEmail": work_email
        }
    }
    api_url = "{0}/scim2/Users/{1}".format(base_url,user_id)
    try:
        results = requests.put(url=api_url, headers=headers, data=json.dumps(payload))
    except:
        raise ValueError
    return results

def get_zoom_scim_user(user_id, token, base_url="https://api.zoom.us"):
    headers = {
        'authorization': "Bearer {0}".format(token),
        'content-type': "application/json"
    }
    api_url = "{0}/scim2/Users/{1}".format(base_url,user_id)
    try:
        results = requests.get(url=api_url, headers=headers)
    except:
        raise ValueError
    return results

def update_zoom_scim_email(user_id, new_email, token, base_url="https://api.zoom.us"):
    headers = {
        'authorization': "Bearer {0}".format(token),
        'content-type': "application/json"
    }
    api_url = "{0}/scim2/Users/{1}".format(base_url,user_id)
    payload = { 'emails':
        [
            {
                'type': "work",
                'value': new_email,
                'primary': True
            }
        ]
    }

    try:
        results = requests.put(url=api_url, data=json.dumps(payload), headers=headers)
    except:
        raise ValueError
    return results


