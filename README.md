# zoom_tools

### Overview

[homepage](https://bitbucket.org/codeforpenn/penn_zoom_tools/src/master/)

This code allows for management of a Zoom Enterprise account.

This code currently includes functions that interact directly with the Zoom API endpoints and don't rely on an externally maintained API wrapper or most other modules.

This code does rely on a recommended package, pyjwt, from Zoom for managing JWT tokens.

Zoom documentation recommend packages available here:
https://jwt.io/#libraries-io

This code also depends on Requests, because while I don't like too many dependencies in code involved in user management, it has proven a stable and defacto tool in Python across many versions


### A Note about API Access and Zoom

OAuth access is available for application types that feature interactive user authorization. Most of the tools contained herein would not fall under that category.

For background processes and the Zoom API you must generate a JWT token.

https://marketplace.zoom.us/docs/api-reference/using-zoom-apis

To generate this token an admin must create a private marketplace JWT application. 

This application can configure a JWT client key and secret. You can use these values to generate a JWT Token. 

Once this token is generated, it is the ONLY token available for the enterprise account.

This token has full administrative access.

You can neither scope token authorization nor provision additional tokens.


### Zoom API Documentation

https://marketplace.zoom.us/docs/api-reference/introduction

### Zoom API SCIM Endpoints Documentation

https://marketplace.zoom.us/docs/api-reference/scim-api


### Dependencies

* python3
* requests
* pyjwt


### Author ###

* jdenk@upenn.edu
